#!/bin/bash



function checkBranch() {
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$BRANCH" != "tempReportCard" ]]; then
git checkout tempReportCard
fi
}

function addCommit() {
git add . 
git commit -m " $1 marks updated "

}

function addEnglish() {
checkBranch
studentName=$1
engMarks=$2
sed -i "/^$studentName/ s/$/ english=$engMarks/" ReportCard.txt
addCommit $1
}


function addHindi() {
checkBranch
studentName=$1
hindiMarks=$2
sed -i "/^$studentName/ s/$/ hindi=$hindiMarks/" ReportCard.txt
addCommit $1
}


function addMaths() {
checkBranch
studentName=$1
mathsMarks=$2
sed -i "/^$studentName/ s/$/ maths=$mathsMarks/" ReportCard.txt
addCommit $1
}

function publishToMainReportCard() {

BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$BRANCH" != "mainReportCard" ]]; then
git checkout mainReportCard
git merge tempReportCard
else
git merge tempReportCard
fi

}

function showHistory() {
    branchName=$1
    if [[ "$branchName" == tempReportCard ]]; then
    git log -p tempReportCard  ReportCard.txt
    elif [[ "$branchName" == mainReportCard ]]; then
    git log -p mainReportCard  ReportCard.txt
    fi

}

function deleteEntries() {
    branchName=$1
    noOfEntries=$2
    if [[ "$branchName" == tempReportCard ]]; then
    git checkout tempReportCard
    git reset --hard HEAD~$noOfEntries
    #git revert HEAD~$noOfEntries
    elif [[ "$branchName" == mainReportCard ]]; then
    git checkout mainReportCard
    git reset --hard HEAD~$noOfEntries
    #git revert HEAD~$noOfEntries
    fi
}



choice=$1
case $choice in 

addEnglish)
addEnglish $2 $3 ;;

addHindi)
addHindi $2 $3 ;;

addMaths)
addMaths $2 $3 ;;

publishToMainReportCard)
publishToMainReportCard ;;

showHistory)
showHistory $2 $3 ;;

deleteEntries)
deleteEntries $2 $3 ;;


esac
